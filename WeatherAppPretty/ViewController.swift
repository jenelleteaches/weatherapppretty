//
//  ViewController.swift
//  WeatherAppPretty
//
//  Created by robin on 2018-07-15.
//  Copyright © 2018 robin. All rights reserved.
//

import UIKit
import Alamofire            // 1. Import AlamoFire and SwiftyJson
import SwiftyJSON

import ChameleonFramework         // optional - used to make colors pretty

import CoreLocation     // need this to get the current locaiton




class ViewController: UIViewController, CLLocationManagerDelegate {

    // User interface outlets
    
    
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTemp: UILabel!
    @IBOutlet weak var imageWeatherIcon: UIImageView!
    @IBOutlet weak var labelHumidity: UILabel!
    @IBOutlet weak var labelVisibility: UILabel!
    @IBOutlet weak var labelRain: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    
    // 2. Specify which URL you want to visit
    let URL = "https://api.darksky.net/forecast/ff41689fc242d7783a79fab7ae586b2b/"
    let PARAMS = "?exclude=minutely,hourly,daily,alerts,flags&units=ca"

    
    // TODO: location variables
    let locationManager : CLLocationManager = CLLocationManager()
    lazy var geocoder = CLGeocoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // UI NONSENSE - Make a gradient background
        
        let colors:[UIColor] = [
            UIColor(hexString:"#F27121")!,
            UIColor(hexString:"#E94057")!,
            UIColor(hexString:"#8A2387")!
        ]
        view.backgroundColor = UIColor(gradientStyle:UIGradientStyle.topToBottom, withFrame:view.frame, andColors:colors)


        // LOGIC - Get the current location
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        
        self.locationManager.startUpdatingLocation()
        
        
        
        
        // LOGIC - Get the weather
        let lat = locationManager.location?.coordinate.latitude
        let lng = locationManager.location?.coordinate.longitude
        getWeather(latitude:"\(lat!)", longitude:"\(lng!)")
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func refreshWeather(_ sender: Any) {
        let lat = locationManager.location?.coordinate.latitude
        let lng = locationManager.location?.coordinate.longitude
        
        
        print("=====\(lat), \(lng)======")
        
        
        // update the location textbox
        
        var location = locationManager.location!
        
        geocoder.reverseGeocodeLocation(location) {
            (placemarks, error) in
            
            // put code here
            print("reversed!)")
            if let error = error {
                print("Unable to Reverse Geocode Location (\(error))")
                print("Cannot find address for this location")
                
            } else {
                if let placemarks = placemarks, let placemark = placemarks.first {
                    print(placemark.locality!)
                    self.labelLocation.text = placemark.locality!
                } else {
                    print("No Matching Addresses Found")
                }
            }
            
        }
        /*
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {
            (placemarks, error) in
            
            print(location)
            
            if error != nil {
                print("Reverse geocoder failed with error" + error.localizedDescription)
                return
            }
            
            if placemarks.count > 0 {
                let pm = placemarks[0] as! CLPlacemark
                println(pm.locality)
            }
            else {
                println("Problem with the data received from geocoder")
            }
        })
        */
        
        return
        
        
        // update the weather
        getWeather(latitude:"\(lat!)", longitude:"\(lng!)")
        
    }

    @IBAction func changeLocationPressed(_ sender: Any) {
    
        /*
        print("reqeuesting new weather data")
        // create the alert box
        let alertBox = UIAlertController(title: "DEF", message: "What color is the sky?", preferredStyle: .alert)
        
        // add a text box
        alertBox.addTextField()
        
        
        // create an Save button
        let saveButton = UIAlertAction(title: "OK", style: .default, handler: {
            
            action in
            
            let item = alertBox.textFields?[0].text
            print("item in textbox: \(item)")
            if item!.isEmpty == true {
                return
            }
            
            
            if (item == "blue") {
                print("you win!")
            }
            else {
                print("you lose!")
            }
            alertBox.addAction(saveButton)
            
            // show the alert box
            present(alertBox, animated:true)
            */
    }
    
    
    func getWeather(latitude:String, longitude:String) {
        // Build the URL:
        
        
        let url = "\(URL)\(latitude),\(longitude)\(PARAMS)"
        print(url)
        
        
        
        // 3. Setup your AlamoFire request
        Alamofire.request(url, method: .get, parameters: nil).responseJSON {
            
            // 4. Note - this is a closure and it runs aynchronously
            // A closure is a "function inside another function"
            response in
            if response.result.isSuccess {
                // 5. If you are successful in getting a JSON response, do something
                if let dataFromServer = response.data {
                    
                    // 6. This is how to use SwiftyJSON to parse the JSON response
                    do {
                        let json = try JSON(data: dataFromServer)
                        
                        let lat = json["latitude"].double!
                        let lng = json["longitude"].double!
                        let tz = json["timezone"].string!
                        
                        
                        print(lat)
                        print(lng)
                        print(tz)
                        
                        let curr = json["currently"].dictionaryValue
                        
                        let time = (curr["time"]!.double)!
                        print(time)
                        
                        let icon = curr["icon"]!.stringValue
                        let status = curr["summary"]!.stringValue
                        
                        
                        
                        
                        let date = Date(timeIntervalSince1970: time)
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
                        //dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
                        
                        dateFormatter.dateFormat = "EEEE MMMM d, YYYY" //Specify your format that you want
                        let strDate = dateFormatter.string(from: date)
                        
                        print(strDate)
                        
                        
                        //update bar at top
                        self.title = strDate
                        
                        dateFormatter.dateFormat = "h:mm a"
                        let strTime = dateFormatter.string(from:date)
                        
                        
                        
                        
                        let temp = String(format: "%.0f", (curr["temperature"]?.doubleValue)!)
                        print(temp)
                        let humidity = curr["humidity"]
                        let uv = curr["uvIndex"]
                        let chanceOfRain = curr["precipProbability"]

                        
                        
                        // so we got all the data, now let's update the UI
                        self.labelDate.text = "Last Updated:  \(strTime)"
                        self.labelTemp.text = "\(temp)°"
                        self.labelHumidity.text = "\(humidity!)"
                        self.labelVisibility.text = "\(uv!)"
                        self.labelRain.text = "\(chanceOfRain!)"
                        self.labelStatus.text = "\(status)"
                        
                        self.weatherIcon.image = UIImage(named: "\(icon).png")
                        
                        
                        print("\(icon).png")
                        
                        
                    }
                    catch {
                        print("error")
                    }
                    
                }
                else {
                    print("Error when getting JSON from the response")
                }
            }
            else {
                // 6. You got an error while trying to connect to the API
                print("Error while fetching data from URL")
            }
            
        }
        

    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // this function gets called every time the person's location changes
        print("location updated!")
        // get the last location in the array
        let i = locations.count - 1;
        let location = locations[i]
        
        /*
        print("Last location: \(location)")
        
        // get latitude and longitude
        let lat = location.coordinate.latitude
        let lng = location.coordinate.longitude

        print("new location: \(lat), \(lng)")
         */
        
        //self.locationManager.stopUpdatingLocation()
        
        
    }
    

}

